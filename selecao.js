


// ---------------------------Questão 1----------------------------
/*
* Faz a soma de todos os elementos da sequencia ate o indice
*/
/**
 * 
 * @param {number} X 
 * @return {char} retorna a soma da sequencia de 0 a X
 */
function sum(X){
  let result=0;
  for(let i=0;i<=X;i++){
    result += i;
  }
  return result;
}

/*
* Mostra o cabeçalho e resposta da quentão em ASCII
*/
function quest1(){
  let X = prompt("Qual indice do elemento da sequencia {A,C,F,J,...}, você deseja?");
  console.log("Questão 1\n");
  console.log("Resposta: "+String.fromCharCode(64+sum(X)));//ascII + soma dos anteriores
}

// --------------------------Questão 2-----------------------------
/*
* Soma os elementos do array a e b, e atribui x aonde tem 'x' nos arrays
*/
/**
 * 
 * @param {number} x numero a ser adicionado aos arrays a e b
 * @return {array} soma de todos os elementos do array a e array b
 */
function comput(x){
  let result=[];
  a=[1,2,3,'x',8,9];
  b=['x',6,5,4];
  let eq = (sum, current) => sum + current;
  
  a[a.indexOf('x')] = x
  b[b.indexOf('x')] = x
  
  result.push(a.reduce(eq))
  result.push(b.reduce(eq))
  return result
}

/*
* Mostra o cabeçalho e resposta da quentão
*/
function quest2(){
  console.log("Questão 2\n");
  console.log("Resposta: "+ comput(7)); //o 7 foi um valor encontrado na questão 2
}

// --------------------------Questão 3-----------------------------
/*
* Procura elementos duplicados e me retorna um array com seus indices
*/
/**
 * 
 * @param {array} dado lista de valores interios
 * @return {array} retorna um array com os indices das segundas ocorrencias de cada valor
 */
function findDuplicate(dado){
  let result=[];
  for(let i=0;i<dado.length;i++){ 
    if(dado.lastIndexOf(dado[i]) != i){
      result.push(dado.lastIndexOf(dado[i])+1);
    }
  }
  return result;
}

/*
* retorna  o primeiro número duplicado para o qual a segunda ocorrência
* possui o  índice  mínimo
*/
/**
 * 
 * @param {array} a lista de valores inteiros
 * @return {number} primeiro número duplicado para o qual a segunda ocorrência
* possui o  índice  mínimo.
* @return {number} '-1' se não ocorrer valor duplicado.
 */
function locateDuplicate(a){
  let result=findDuplicate(a);
  return (result.length == 0 ? -1 : a[Math.min(...result)-1]);
}

/**
 * 
 * @param {array} a lista de valores interios
 * @return {array} lista de valores inteiros sem suplicatas
 */
function removeDuplicate(a){
  let aux=findDuplicate(a);
  for(let i=0;i<aux.length;i++){
    a.splice(aux[i]-1,1);
  }
  return a;
}
/*
* Mostra o cabeçalho e resposta da quentão
*/
function quest3(){
  let a = [2, 1, 3, 5, 3, 2];
  console.log("Questão 3");
  console.log(a);
  console.log("locateDuplicate(a)="+locateDuplicate(a));
  console.log("removeDuplicate(a)="+removeDuplicate(a));
}

// --------------------------Questão 4-----------------------------
/*
* Faz a comparação da versão, comparando o 'major', 'minor', 'patch' e o 'build',
* em que, começando do 'minor', o primeiro a apresentar diferença, gera a saida
*/
/**
 * @param {string} versionA 
 * @param {string} versionB 
 * @return {number} 1 se versionA>versionB
 * @return {number} -1 se versionA<versionB
 * @return {number} 0 se versionA==versionB. 
 */
function compareVersion(versionA,versionB){
  let Va = versionA.split('.');
  let Vb = versionB.split('.');
  let result=0;

  for(let i=0;i<Va.length;i++){
    if(Va[i]<Vb[i]){
      result = -1
      break;
    }
    else if(Va[i]>Vb[i]){
      result = 1
      break;
    }
  }
  return result;
}
/*
* Mostra o cabeçalho e resposta da quentão
*/
function quest4(){
  const versionA  = "1.5.4.0";
  const versionB  = "1.5.2.0";

  console.log("Questão 4<br>");
  console.log("compareVersion(vA,vB)="+compareVersion(versionA,versionB));
}

// --------------------------Questão 5-----------------------------
function newXmlHttpRequest() {
  if (window.XMLHttpRequest) {
    return new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    return new ActiveXObject("Microsoft.XMLHTTP");
  }
}

/**
 * 
 * @param {string} evento com o nome da pessoa a ser pesquisada
 */
function quest5(e){
  let xhttp = newXmlHttpRequest();
  let user = e.value
  let eq = (sum, current) => sum + current.stargazers_count;
  let img = new Image(100,100);

  xhttp.onreadystatechange = function() {
    if (this.status === 200 & this.readyState === 4){
      const resposta = JSON.parse(this.responseText);
      const repos = "Repositorios: " + resposta.length;
      const lista = resposta.map(i => `<a href="${i.html_url}"><li> ${i.name}</li></a>`).join('') ;
      const stars = "Estrelas: " + resposta.reduce(eq,0);

      img.src = resposta[0].owner.avatar_url;
      img.style.borderRadius = "20px";

      const NodeImage = document.querySelector("#gitAvatar");
      if(NodeImage.childNodes.length == 0){ //se já existir imagem, substitua
        NodeImage.appendChild(img);
      }
      else{
        NodeImage.replaceChild(img,NodeImage.childNodes[0]);
      }
      document.querySelector("#gitRepo").innerHTML = repos;  
      document.querySelector("#gitStar").innerHTML = stars; 
      document.querySelector(".gitList").innerHTML = lista;
    }
    else if(this.status >=400){
      document.querySelector(".gitList").innerHTML = "[ERRO] Pesquisa apresentou problemas, procure o suporte.";
    }
  };
  xhttp.open("GET", "https://api.github.com/users/"+user+"/repos", true);
  xhttp.send();
}

// --------------------------Questão 6-----------------------------
//Note "desenvolver o código-fonte usando apenas html e css"


// --------------------------Questão 7-----------------------------
/**
 * 
 * @param {JSON} lista 
 * {
 * contatos: [ 
 *    id: number, 
 *    name: string, 
 *    telefone: string, 
 *    email: string
 *  ]
 * }
 */
function CreateAgenda(lista){
  const result = lista.contatos.map(dado =>
    `<fieldset class="cardField" id="contato${dado.id}">
    <span>Nome: </span>
    <input type="text" id="contato${dado.id}_name" value=${dado.name}>  
    <br>
    <span>Telefone: </span>
    <input type="text" id="contato${dado.id}_telefone" value=${dado.telefone}>  
    <br>
    <span>Email: </span>
    <input type="text" id="contato${dado.id}_email"value=${dado.email}>  
    <br>
    <input type="button" value="Deletar" onclick={DeleteAgenda(${dado.id})}>
    <input type="button" value="Salvar" onclick={PutAgenda(${dado.id})}>
    </fieldset> `
  ).join('');
  return result;
}

function SaveAgenda(){
  let filename = 'db.json';
  var element = document.createElement('a');

  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(JSON.stringify(Agenda)));
  element.setAttribute('download', filename);
  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

var Agenda;
function SearchAgenda(e){
  let name = e.value;
  if(name.length & Agenda!=undefined){
    Agenda.contatos.map(dado =>{
      if(dado.name == name){
        let contato = CreateAgenda({contatos:[dado]});
        document.getElementById("ListAgenda").innerHTML = contato;
        return 0;
      }
    });
  }
}

/**
* lista todos os contatos da agenda em db.json
*/
function GetAgenda(e){
  let rawFile = e.target;
  let reader = new FileReader();
  reader.onload = function(){
      Agenda = JSON.parse(this.result); //Global
      const dadosAgenda = CreateAgenda(Agenda);
      document.getElementById("ListAgenda").innerHTML = dadosAgenda;

      document.getElementById("openAgenda").hidden = true;            
      document.getElementById("newAgenda").hidden = false;            
      document.getElementById("saveAgenda").hidden = false;            
      document.getElementById("searchBox").hidden = false;            
  }
  reader.readAsText(rawFile.files[0]);
}

/**
 * 
 * @param {number} e id do contato a ser deletado da agenda
 */
function DeleteAgenda(e){
  Agenda.contatos.map((dado,index) =>{
    if(dado.id == e){
      Agenda.contatos.splice(index,1);
      return 0;
    }
  });
  const contato = document.getElementById("contato"+e);
  document.getElementById("ListAgenda").removeChild(contato);

  console.log("Nova Agenda",Agenda);
}

/**
 * altera um contato na agenda
 * @param {number} e id do contato a ser auterado
 */
function PutAgenda(e){
  const contato = document.getElementById("contato"+e);
  Agenda.contatos.map(dado =>{
    if(dado.id == e){
      dado.name = contato.children.namedItem("contato"+e+"_name").value;
      dado.telefone = contato.children.namedItem("contato"+e+"_telefone").value;
      dado.email = contato.children.namedItem("contato"+e+"_email").value;
      return 0;
    }
  });

  console.log("Nova Agenda",Agenda);
}

function PostAgenda(){
  let id = Agenda.contatos[Agenda.contatos.length-1].id+1;
  Agenda.contatos.push({"id":id})  //adiciona na agenda

  id = {'contatos':[{'id':id}]};
  let contato = document.createElement('div');
  contato.innerHTML = CreateAgenda(id); 
  document.getElementById("ListAgenda").appendChild(contato.firstChild);   //adiciona na view
}


// --------------------------Questão 8-----------------------------
/*
* Problema de escopo, usar uma declaração em escopo como "let" ou usar
* outra forma de percorrer os elementos de "as", como por exemplo o
* uso do "map()"
*/
function registerLinks(){
  var as = document.getElementsByTagName('a');
  // for (let i = 0; i < as.length; i++) {
  //   as[i].onclick = function() {
  //     alert(i); 
  //     return false; 
  //   } 
  // } 
  Array.prototype.map.call(as,(i,index) => {
  i.onclick = function() {
      alert(index); 
      return false; 
    }
  });
}

// --------------------------Questão 9-----------------------------
/**
 * @prop {string} clientid
 * @return {number} the ckeck digit
 */
function digitoVerificador(clientid){
  let eq = (sum, current) => sum + current;
  clientid = String(clientid);

  if(clientid.length>1){
    var digit = clientid.split("").map(Number).reduce(eq);//divide a string, cast para int e soma todos
    digitoVerificador(String(digit));
  }
  return digit;
}

// --------------------------Questão 10-----------------------------
/**
 * @prop {array a, array b}
 * @return [number,number] Pontuaçao da pessoa 0 e a pessoa 1
 * @return [null,null] se o numero de pontos entre as pessoas, forem diferentes
 */
function compareElements(a,b){
  let aPoints=0;
  let bPoints=0;
  try{
    if(a.length !== b.length) throw "Quantidades de pontos devem ser iguais.";
    for (let i=0;i<a.length;i++){
      aPoints += (a[i]>b[i] ? 1 : 0);
      bPoints += (a[i]<b[i] ? 1 : 0);
    }
    return [aPoints,bPoints];
  }
  catch(err){
    alert(err);
    return [null,null];
  }
}






